rem Feel free to customize this
set MINGWDIR=c:\mingw

mkdir %MINGWDIR%

mkdir cache
xcopy Tools\mingw-get-0.6.2-mingw32-beta-20131004-1-bin.tar.xz cache
cd cache
..\Tools\xz -d *.xz
for %%i in (*.tar) do ..\Tools\tar -xf %%i -C %MINGWDIR%
del /q *.tar
cd ..

rem Install python
cd Tools
WGET.exe --no-check-certificate https://www.python.org/ftp/python/2.7.8/python-2.7.8.msi
python-2.7.8.msi
xcopy python2.7-config c:\Python27
cd ..

rem Install all the required packages
cd %MINGWDIR%\bin
mingw-get.exe update
mingw-get.exe install mingw-get
copy ..\var\lib\mingw-get\data\defaults.xml ..\var\lib\mingw-get\data\profile.xml
mingw-get.exe install mingw32-gcc
mingw-get.exe install mingw32-gcc-g++
mingw-get.exe install mingw32-pexports
mingw-get.exe install mingw32-libexpat
mingw-get.exe install mingw32-expat
mingw-get.exe install mingw-developer-toolkit
mingw-get.exe install msys-wget
mingw-get.exe install msys-unzip
mingw-get.exe install msys-libiconv
mingw-get.exe install msys-libexpat
mingw-get.exe install msys-expat

rem the latest version of gettext is broken, see: http://stackoverflow.com/questions/23637991/building-lzma-linker-errors
cd %MINGWDIR%\lib
del libasprintf.la libgettextlib.la libgettextpo.la libgettextsrc.la libintl.la

rem Set the right exports
set PREFIX=opt

echo export PREFIX="/%PREFIX%" >> %MINGWDIR%\msys\1.0\etc\profile

mkdir %MINGWDIR%\msys\1.0\%PREFIX%
mkdir %MINGWDIR%\msys\1.0\%PREFIX%\bin
mkdir %MINGWDIR%\msys\1.0\%PREFIX%\etc
mkdir %MINGWDIR%\msys\1.0\%PREFIX%\include
mkdir %MINGWDIR%\msys\1.0\%PREFIX%\lib
mkdir %MINGWDIR%\msys\1.0\%PREFIX%\lib\pkgconfig
mkdir %MINGWDIR%\msys\1.0\%PREFIX%\share


echo export PATH="$PATH:/%PREFIX%/bin:/c/Python27" >> %MINGWDIR%\msys\1.0\etc\profile
echo export CFLAGS="-I/%PREFIX%/include" >> %MINGWDIR%\msys\1.0\etc\profile
echo export CPPFLAGS="-I/%PREFIX%/include" >> %MINGWDIR%\msys\1.0\etc\profile
echo export LDFLAGS="-L/%PREFIX%/lib" >> %MINGWDIR%\msys\1.0\etc\profile
echo export PKG_CONFIG_PATH="/%PREFIX%/lib/pkgconfig" >> %MINGWDIR%\msys\1.0\etc\profile
echo export XDG_DATA_DIRS="/%PREFIX%/share" >> %MINGWDIR%\msys\1.0\etc\profile
echo export ACLOCAL_FLAGS="-I /opt/share/aclocal" >> %MINGWDIR%\msys\1.0\etc\profile
echo export ACLOCAL_PATH="/opt/share/aclocal" >> %MINGWDIR%\msys\1.0\etc\profile
echo export LIBRARY_PATH="/opt/lib" >> %MINGWDIR%\msys\1.0\etc\profile
