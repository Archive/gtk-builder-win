MODVER=1.1.28

mkdir -p 4_libxslt
cd 4_libxslt
test -f libxslt-$MODVER.tar.gz || wget http://xmlsoft.org/sources/libxslt-$MODVER.tar.gz
gzip -d -f -c libxslt-$MODVER.tar.gz > libxslt-$MODVER.tar
tar -xf libxslt-$MODVER.tar
cd libxslt-$MODVER


echo Compile...

./configure --prefix=$PREFIX
make clean
make 2>&1 | tee ../../logs/4_libxslt-make.log
make install 2>&1 | tee ../../logs/4_libxslt-makeinstall.log


cd ..
rm -rf libxslt-$MODVER
rm -f libxslt-$MODVER.tar
