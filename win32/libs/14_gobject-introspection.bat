MAJOR_VER=1
MINOR_VER=40
PATCH_VER=0
MODVER=$MAJOR_VER.$MINOR_VER.$PATCH_VER

mkdir -p 14_gobject-introspection
cd 14_gobject-introspection
test -f gobject-introspection-$MODVER.tar.xz || wget http://ftp.acc.umu.se/pub/gnome/sources/gobject-introspection/$MAJOR_VER.$MINOR_VER/gobject-introspection-$MODVER.tar.xz
xz -d -k -f gobject-introspection-$MODVER.tar.xz
tar -xf gobject-introspection-$MODVER.tar
cd gobject-introspection-$MODVER


echo Compile...

./configure --prefix=$PREFIX
make clean
make 2>&1 | tee ../../logs/14_gobject-introspection-make.log
make install 2>&1 | tee ../../logs/14_gobject-introspection-makeinstall.log


cd ..
rm -rf gobject-introspection-$MODVER
rm -f gobject-introspection-$MODVER.tar
