MAJOR_VER=3
MINOR_VER=12
PATCH_VER=2
MODVER=$MAJOR_VER.$MINOR_VER.$PATCH_VER

mkdir -p 53_gtksourceview3
cd 53_gtksourceview3
test -f gtksourceview-$MODVER.tar.xz || wget http://ftp.acc.umu.se/pub/gnome/sources/gtksourceview/$MAJOR_VER.$MINOR_VER/gtksourceview-$MODVER.tar.xz
xz -d -k -f gtksourceview-$MODVER.tar.xz
tar -xf gtksourceview-$MODVER.tar
cd gtksourceview-$MODVER

echo Compile...

./configure --prefix=$PREFIX
make clean
make 2>&1 | tee ../../logs/53_gtksourceview3-make.log
make install 2>&1 | tee ../../logs/53_gtksourceview3-makeinstall.log


export CFLAGS="$CFLAGS_SAVE"
unset CFLAGS_SAVE
cd ..
rm -rf gtksourceview-$MODVER
rm -f gtksourceview-$MODVER.tar
