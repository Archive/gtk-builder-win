MODVER=1.11.6

mkdir -p 4_automake
cd 4_automake
test -f automake-$MODVER.tar.gz || wget http://ftp.gnu.org/gnu/automake/automake-$MODVER.tar.gz
gzip -d -f -c automake-$MODVER.tar.gz > automake-$MODVER.tar
tar -xf automake-$MODVER.tar
cd automake-$MODVER


echo Compile...

# note install in the same prefix as mingw
./configure --prefix=/mingw
make clean
make 2>&1 | tee ../../logs/4_automake-make.log
make install 2>&1 | tee ../../logs/4_automake-makeinstall.log


cd ..
rm -rf automake-$MODVER
rm -f automake-$MODVER.tar
