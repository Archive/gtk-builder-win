MODVER=2.11.1

mkdir -p 33_libfontconfig
cd 33_libfontconfig
test -f fontconfig-$MODVER.tar.gz || wget http://www.freedesktop.org/software/fontconfig/release/fontconfig-$MODVER.tar.gz
gzip -d -f -c fontconfig-$MODVER.tar.gz > fontconfig-$MODVER.tar
tar -xf fontconfig-$MODVER.tar
cd fontconfig-$MODVER

# https://bugs.freedesktop.org/show_bug.cgi?id=81228
cd ..
patch -p0 < fcatomic.patch
cd fontconfig-$MODVER

echo Compile...

./configure --prefix=$PREFIX
make clean
make 2>&1 | tee ../../logs/33_libfontconfig-make.log
make install 2>&1 | tee ../../logs/33_libfontconfig-makeinstall.log

echo Doc installation partially fails at the end
echo so we install the .pc file manually

cp fontconfig.pc $PREFIX/lib/pkgconfig


cd ..
rm -rf fontconfig-$MODVER
rm -f fontconfig-$MODVER.tar
